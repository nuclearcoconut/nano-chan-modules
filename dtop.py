import sopel.module
import sqlite3 as lite
from urllib.parse import urlparse

@sopel.module.commands('dtop|desktop')
def dtop(bot, trigger):
    con = lite.connect('/Users/ebrock/.sopel/nano-chan/nano-chan.db')
    c = con.cursor()
    if trigger.group(2) is None:
        args = 0
    else:
        args = trigger.group(2).strip().split(' ')

    if(args == 0):
        cur = c.execute('SELECT dtop FROM dtops WHERE nick=?', (trigger.nick,))
        dtops = enumerate(cur.fetchall())
        out = trigger.nick + ':'
        for dtop in dtops:
            out += ' [{}] {}'.format(dtop[0] + 1, dtop[1][0])
        if(out == trigger.nick + ':'):
            bot.say('No desktops found...')
        else:
            bot.say(out)
    elif((args[0] == '-a') | (args[0] == '--add')):
        for url in args[1:]:
            if(urlparse(url).scheme != ''):
                c.execute('INSERT INTO dtops VALUES (?, ?)', (trigger.nick, url))
            else:
                bot.say("That's not a valid url, you dummy!")
            if(con.total_changes > 1):
                bot.say('Desktops added!')
            elif(con.total_changes > 0):
                bot.say('Desktop added!')
            else:
                bot.say('No desktops added...')
            con.commit()
    elif((args[0] == '-d') | (args[0] == '--delete')):
        cur = c.execute('SELECT dtop FROM dtops WHERE nick=?', (trigger.nick,))
        dtops = cur.fetchall()
        if(args[1] == '*'):
            c.execute('DELETE FROM dtops WHERE nick=?', (trigger.nick,))
        else:
            for dtopid in args[1:]:
                c.execute('DELETE FROM dtops WHERE nick=? AND dtop=?', (trigger.nick, dtops[int(dtopid) - 1]))
        if(con.total_changes > 1):
            bot.say('Desktops deleted!')
        elif(con.total_changes > 0):
            bot.say('Desktop deleted!')
        else:
            bot.say('No desktops deleted...')
        con.commit()
    elif((args[0] == '-r') | (args[0] == '--replace')):
        cur = c.execute('SELECT dtop FROM dtops WHERE nick=?', (trigger.nick,))
        dtops = cur.fetchall()
        if(urlparse(args[2]).scheme != ''):
            c.execute('UPDATE dtops SET dtop=? WHERE nick=? AND dtop=?', (args[2], trigger.nick, dtops[int(args[1]) - 1]))
        else:
            bot.say("That's not a valid url, you dummy!")
        if(con.total_changes > 0):
            bot.say('Desktop replaced')
            con.commit()
        else:
            bot.say('No desktop replaced...')
    elif(args[0].isdigit()):
        cur = c.execute('SELECT dtop FROM dtops WHERE nick=?', (trigger.nick,))
        dtops = cur.fetchall()
        if dtops[args[0]] is None:
            bot.say('No desktop with that ID found...')
        else:
            bot.say(dtops[args[0] - 1])
    else:
        cur = c.execute('SELECT dtop FROM dtops WHERE nick=?', (args[0],))
        dtops = enumerate(cur.fetchall())
        out = args[0] + ':'
        if dtops is None:
            bot.say('No desktops found...')
        else:
            for dtop in dtops:
                out += ' [{}] {}'.format(dtop[0], dtop[1][0])
                bot.say(out)
