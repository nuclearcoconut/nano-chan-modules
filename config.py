import sopel.module
import sqlite3 as lite
from urllib.parse import urlparse

@sopel.module.commands('config')
def config(bot, trigger):
    con = lite.connect('/Users/ebrock/.sopel/nano-chan/nano-chan.db')
    c = con.cursor()
    if trigger.group(2) is None:
        args = 0
    else:
        args = trigger.group(2).strip().split(' ')

    if(args == 0):
        cur = c.execute('SELECT config FROM configs WHERE nick=?', (trigger.nick,))
        configs = enumerate(cur.fetchall())
        out = trigger.nick + ':'
        for config in configs:
            out += ' [{}] {}'.format(config[0] + 1, config[1][0])
        if(out == trigger.nick + ':'):
            bot.say('No configs found...')
        else:
            bot.say(out)
    elif((args[0] == '-a') | (args[0] == '--add')):
        for url in args[1:]:
            if(urlparse(url).scheme != ''):
                c.execute('INSERT INTO configs VALUES (?, ?)', (trigger.nick, url))
            else:
                bot.say("That's not a valid url, you dummy!")
            if(con.total_changes > 1):
                bot.say('Configs added!')
            elif(con.total_changes > 0):
                bot.say('Config added!')
            else:
                bot.say('No configs added...')
            con.commit()
    elif((args[0] == '-d') | (args[0] == '--delete')):
        cur = c.execute('SELECT config FROM configs WHERE nick=?', (trigger.nick,))
        configs = cur.fetchall()
        if(args[1] == '*'):
            c.execute('DELETE FROM configs WHERE nick=?', (trigger.nick,))
        else:
            for configid in args[1:]:
                c.execute('DELETE FROM configs WHERE nick=? AND config=?', (trigger.nick, configs[int(configid) - 1]))
        if(con.total_changes > 1):
            bot.say('Configs deleted!')
        elif(con.total_changes > 0):
            bot.say('Config deleted!')
        else:
            bot.say('No configs deleted...')
        con.commit()
    elif((args[0] == '-r') | (args[0] == '--replace')):
        cur = c.execute('SELECT config FROM configs WHERE nick=?', (trigger.nick,))
        configs = cur.fetchall()
        if(urlparse(args[2]).scheme != ''):
            c.execute('UPDATE configs SET config=? WHERE nick=? AND config=?', (args[2], trigger.nick, configs[int(args[1]) - 1]))
        else:
            bot.say("That's not a valid url, you dummy!")
        if(con.total_changes > 0):
            bot.say('Config replaced')
            con.commit()
        else:
            bot.say('No config replaced...')
    elif(args[0].isdigit()):
        cur = c.execute('SELECT config FROM configs WHERE nick=?', (trigger.nick,))
        configs = cur.fetchall()
        if configs[args[0]] is None:
            bot.say('No config with that ID found...')
        else:
            bot.say(configs[args[0] - 1])
    else:
        cur = c.execute('SELECT config FROM configs WHERE nick=?', (args[0],))
        configs = enumerate(cur.fetchall())
        out = args[0] + ':'
        if configs is None:
            bot.say('No configs found...')
        else:
            for config in configs:
                out += ' [{}] {}'.format(config[0], config[1][0])
                bot.say(out)
